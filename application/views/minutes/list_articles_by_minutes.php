<?php

echo '<div style="color:#E1B95A; font-size: 30px; text-align:center; margin: 20px 0;">' . 'Listado de artículos del acta ' . $minutes . '</div>';
?>
<div style="width: 80%; margin: 0 auto;">
    <div style="margin: 20px 0 40px 0;">
        <a style="color: white; text-decoration: none; "class="btn-send" href="<?php echo base_url(); ?>index.php/home/insert_article?minutes=<?php echo $minutes ?>&period=<?php echo $period ?>">Insertar artículos</a>
    </div> 

    <table id="articles-table">
        <thead>
            <tr><td>Número de acta</td><td>Periodo</td><td>Número de artículo</td><td>Descripción del artículo</td><td>Tiene respuesta?</td><td>Fecha de la respuesta</td><td>Acta de respuesta</td><td>Artículo de respuesta</td><td>Acción</td>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#articles-table').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ resultados por página",
                "zeroRecords": "No se encontraron registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Esta acta aún no tiene artículos",
                "infoFiltered": "(Filtrados de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                }
            },
            "ajax": {
                url: "<?php echo base_url('index.php/minutes/list_articles_by_minutes?minutes=') ?>" + "<?php echo $minutes ?>" + "&period=" + "<?php echo $period ?>", //Se está pasando por GET los datos del acta para que solo se muestren los artículos de esta.
                type: 'GET'
            },
        });
    });
</script>