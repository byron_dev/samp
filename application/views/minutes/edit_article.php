<?php 
    //Atributos del formulario
    $form = array (
        'class' => 'form-register'
    );
    //Atributos del botón    
    $btn = array (
        'class' => 'btn-send'
    );
?>
<?php echo validation_errors('<div class="error">', '</div>'); ?>
<?php echo form_open('minutes/edit_article', $form) //Se está llamando a la función edit_article del controlador "minutes" ?>
<?php
    //Atributos del campo número de acta
    $minutes_number = array (
        'name' => 'minutes_number',
        'class' => 'input-100',
        'value' => $minutes_number,
        'readonly' => 'readonly'
    );
    //Atributos del campo periodo
    $period = array (
        'name' => 'period',
        'class' => 'input-100',
        'value' => $period,
        'readonly' => 'readonly'
    );
    //Atributos del campo número de artículo
    $article_number = array (
        'name' => 'article_number',
        'class' => 'input-100',
        'value' => $article_number,
        'readonly' => 'readonly'
    );
    //Atributos del campo descripción del artículo
    $article_description = array (
        'name' => 'article_description',
        'class' => 'input-100',
        'value' => $article_description,
        'readonly' => 'readonly'
    );
    //Atributos del campo fecha de respuesta
    $answer_date = array (
        'name' => 'answer_date',
        'placeholder' => 'Fecha de la respuesta',
        'class' => 'input-100',
        'type' => 'date'
    );
    //Atributos del campo número de acta de respuesta
    $answer_minutes_number = array (
        'name' => 'answer_minutes_number',
        'placeholder' => 'Número de acta de respuesta',
        'class' => 'input-100'
    );
    //Atributos del campo número de artículo de respuesta
    $answer_article_number = array (
        'name' => 'answer_article_number',
        'placeholder' => 'Número de artículo de respuesta',
        'class' => 'input-100'
    );

?>
<h2 class="form__tittle">Editar artículo</h2>
<div class="inputs-container">
    <?php echo form_label('Número de acta:', 'minutes_number'); ?>
    <?php echo form_input($minutes_number); ?>

    <?php echo form_label('Periodo:', 'period'); ?>
    <?php echo form_input($period); ?>

    <?php echo form_label('Artículo:', 'article_number'); ?>
    <?php echo form_input($article_number); ?>

    <?php echo form_label('Descripción:', 'article_description'); ?>
    <?php echo form_textarea($article_description); ?>

    <?php echo form_label('Tiene respuesta?', 'have_answer'); ?>
    <select class="input-100" name="have_answer">
        <option value="1">Sí</option>
        <option value="0">No</option>
    </select>

    <?php echo form_label('Fecha de respuesta:', 'answer_date'); ?>
    <?php echo form_input($answer_date); ?>

    <?php echo form_label('Acta de respuesta:', 'answer_minutes_number'); ?>
    <?php echo form_input($answer_minutes_number); ?>

    <?php echo form_label('Periodo municipal:', 'municipal_period'); ?>
    <select class="input-100" name="municipal_period">
        <option value="">Seleccione un periodo municipal</option>
        <?php
            foreach($periods as $period) { //Se recorre el array "periods" que fue pasado desde el controlador.
                echo '<option value="'.$period['start_year'] . '">' . $period['period'] . '</option>';
		    }
        ?>
    </select>

    <?php echo form_label('Artículo de respuesta:', 'answer_article_number'); ?>
    <?php echo form_input($answer_article_number); ?>

    <?php echo form_submit('', 'Editar', $btn) ?>
</div>
<?php echo form_close(); ?>