<?php

echo '<div style="color:#E1B95A; font-size: 30px; text-align:center; margin: 20px 0;">' . 'Listado de actas en el sistema' . '</div>';

?>
<div style="width: 80%; margin: 0 auto;">
    <table id="minutes-table">
        <thead>
            <tr><td>Número de acta</td><td>Periodo</td><td>Fecha de la sesión</td><td>Número de sesión</td><td>Tipo de sesión</td><td>Documento</td><td>Ver Artículos</td><td>Agregado por</td>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#minutes-table').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ resultados por página",
                "zeroRecords": "No se encontraron registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(Filtrados de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                }
            },
            "ajax": {
                url: "<?php echo base_url('index.php/minutes/list_minutes') ?>",
                type: 'GET'
            },
            "columnDefs": [ {
                "targets": 5,
                "data": 5,
                "render": function ( data, type, row, meta ) {
                    return '<a href="<?php echo base_url('index.php/minutes/download_file?file=') ?>' + data + '">Descargar archivo</a>';
                }
            } ]
        });
    });
</script>