<?php 
    //Atributos del formulario
    $form = array (
        'class' => 'form-register'
    );
    //Atributos del botón    
    $btn = array (
        'class' => 'btn-send'
    );
?>
<?php echo validation_errors('<div class="error">', '</div>'); ?>
<?php echo form_open_multipart('minutes/insert_minutes', $form) //Se está llamando a la función insert_minutes del controlador "minutes" ?>
<?php
    //Atributos del campo numero de acta
    $minutes_number = array (
        'name' => 'minutes_number',
        'placeholder' => 'Número de acta',
        'class' => 'input-100'
    ); 
    //Atributos del campo fecha de la sesión
    $session_date = array (
        'name' => 'session_date',
        'placeholder' => 'Fecha de la sesión',
        'class' => 'input-100',
        'type' => 'date'
    );
    //Atributos del campo número de sesión
    $session_number = array (
        'name' => 'session_number',
        'placeholder' => 'Número de sesión',
        'class' => 'input-100'
    );
    //Atributos del campo documento
    $minutes_file = array (
        'name' => 'minutes_file',
        'placeholder' => 'Subir acta',
        'class' => 'input-100',
        'type' => 'file'
    );
?>
<h2 class="form__tittle">Ingreso de actas</h2>
<div class="inputs-container">
    <?php echo form_label('Número de acta:', 'minutes_number'); ?>
    <?php echo form_input($minutes_number); ?>

    <?php echo form_label('Periodo municipal:', 'municipal_period'); ?>
    <select class="input-100" name="municipal_period">
        <option value="">Seleccione un periodo municipal</option>
        <?php
            foreach($periods as $period) { //Se recorre el array "periods" que fue pasado desde el controlador home.
                echo '<option value="'.$period['start_year'] . '">' . $period['period'] . '</option>';
		    }
        ?>
    </select>

    <?php echo form_label('Fecha de la sesión:', 'session_date'); ?>
    <?php echo form_input($session_date); ?>

    <?php echo form_label('Número de sesión:', 'session_number'); ?>
    <?php echo form_input($session_number); ?>

    <?php echo form_label('Tipo de sesión:', 'session_type'); ?>
    <select class="input-100" name="session_type">
        <option value="">Seleccione un tipo de sesión</option>
        <option value="ordinaria">Ordinaria</option>
        <option value="extraordinaria">Extraordinaria</option>
    </select>

    <?php echo form_label('Acta:', 'minutes_file'); ?>
    <?php echo form_input($minutes_file); ?>

    <?php echo form_submit('', 'Guardar', $btn) ?>
</div>
<?php echo form_close(); ?>