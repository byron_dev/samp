<?php

echo '<div style="color:#E1B95A; font-size: 30px; text-align:center; margin: 20px 0;">' . 'Listado de artículos en el sistema' . '</div>';
?>
<div style="width: 80%; margin: 0 auto;">
    <table id="articles-table">
        <thead>
        <tr><td>Número de acta</td><td>Periodo</td><td>Número de artículo</td><td>Descripción del artículo</td><td>Tiene respuesta?</td><td>Fecha de la respuesta</td><td>Acta de respuesta</td><td>Artículo de respuesta</td><td>Acción</td>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#articles-table').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ resultados por página",
                "zeroRecords": "No se encontraron registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(Filtrados de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                }
            },
            "ajax": {
                url: "<?php echo base_url('index.php/minutes/list_articles') ?>",
                type: 'GET'
            },
        });
    });
</script>