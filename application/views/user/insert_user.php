<?php 
    //Atributos del formulario
    $form = array (
        'class' => 'form-register'
    );
    //Atributos del botón    
    $btn = array (
        'class' => 'btn-send'
    );
?>
<?php echo validation_errors('<div class="error">', '</div>'); ?>
<?php echo form_open('user/insert_user', $form) //Se está llamando a la función insert_user del controlador "user" ?>
<?php
    //Atributos del campo nombre
    $user_name = array (
        'name' => 'username',
        'placeholder' => 'Nombre',
        'class' => 'input-100'
    ); 
    //Atributos del campo apellido
    $user_last_name = array (
        'name' => 'lastname',
        'placeholder' => 'Apellidos',
        'class' => 'input-100'
    );
    //Atributos del campo usuario
    $user_nickname = array (
        'name' => 'nickname',
        'placeholder' => 'Usuario',
        'class' => 'input-100'
    );
?>
<h2 class="form__tittle">Ingreso de usuario</h2>
<div class="inputs-container">
    <?php echo form_label('Nombre:', 'username'); ?>
    <?php echo form_input($user_name); ?>

    <?php echo form_label('Apellidos:', 'lastname'); ?>
    <?php echo form_input($user_last_name); ?>

    <?php echo form_label('Usuario:', 'nickname'); ?>
    <?php echo form_input($user_nickname); ?>

    <?php echo form_label('Rol:', 'role'); ?>
    <select class="input-100" name="role">
        <option value="">Seleccione un rol</option>
        <option value="1">Super Administrador</option>
        <option value="2">Administrador</option>
        <option value="3">Consulta</option>
    </select>

    <?php echo form_submit('', 'Crear usuario', $btn) ?>
</div>
<?php echo form_close(); ?>