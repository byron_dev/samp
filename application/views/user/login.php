<div style="margin: auto; width: 200px;">
    <img src='<?php echo base_url(); ?>/assets/img/logo.png' alt="logo municipalidad">
</div>

<?php 
    //Atributos del formulario
    $form = array (
        'class' => 'form-register'
    );
    //Atributos del botón    
    $btn = array (
        'class' => 'btn-send'
    );
?>
<?php echo validation_errors('<div class="error">', '</div>'); ?>
<?php echo form_open('user', $form) //Se está llamando a la función "index" del controlador "user" ?>
<?php 
    //Atributos del campo username
    $name = array (
        'name' => 'username',
        'placeholder' => 'Usuario',
        'class' => 'input-100'
    );
    //Atributos del campo password
    $password = array (
        'name' => 'password',
        'placeholder' => 'Contraseña',
        'class' => 'input-100'
    ); 
?>

<h2 class="form__tittle">Ingresar al sistema</h2>
<div class="inputs-container">
    <?php echo form_label('Usuario:', 'username'); ?>
    <?php echo form_input($name); ?>

    <?php echo form_label('Contraseña:', 'password'); ?>
    <?php echo form_password($password); ?>

    <?php echo form_submit('', 'Ingresar', $btn) ?>
</div>

<?php echo form_close(); ?>