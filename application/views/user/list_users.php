<?php

echo '<div style="color:#E1B95A; font-size: 30px; text-align:center; margin: 20px 0;">' . 'Listado de usuarios en el sistema' . '</div>';

?>
<div style="width: 80%; margin: 0 auto;">
    <table id="users-table">
        <thead>
            <tr><td>Nombre</td><td>Apellidos</td><td>Usuario</td><td>Rol</td>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#users-table').DataTable({
            "language": {
                "lengthMenu": "Mostrar _MENU_ resultados por página",
                "zeroRecords": "No se encontraron registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(Filtrados de _MAX_ registros)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                }
            },
            "ajax": {
                url: "<?php echo base_url('index.php/user/list_users') ?>",
                type: 'GET'
            },
        });
    });
</script>