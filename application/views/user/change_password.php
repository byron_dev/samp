<?php 
    //Atributos del formulario
    $form = array (
        'class' => 'form-register'
    );
    //Atributos del botón    
    $btn = array (
        'class' => 'btn-send'
    );
?>
<?php echo validation_errors('<div class="error">', '</div>'); ?>
<?php echo form_open('user/change_password', $form) //Se está llamando a la función change_password del controlador "user" ?>
<?php
    //Atributos del campo contraseña
    $new_password = array (
        'name' => 'new_password',
        'placeholder' => 'Nueva contraseña',
        'class' => 'input-100'
    ); 
    //Atributos del campo de confirmar la nueva contraseña
    $password_confirmation = array (
        'name' => 'password_confirmation',
        'placeholder' => 'Confirme la nueva contraseña',
        'class' => 'input-100'
    );
?>
<h2 class="form__tittle">Cambiar contraseña</h2>
<div class="inputs-container">
    <?php echo form_label('Nueva contraseña:', 'new_password'); ?>
    <?php echo form_password($new_password); ?>

    <?php echo form_label('Confirmar contraseña:', 'password_confirmation'); ?>
    <?php echo form_password($password_confirmation); ?>

    <?php echo form_submit('', 'Cambiar contraseña', $btn) ?>
</div>
<?php echo form_close(); ?>