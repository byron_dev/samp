<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Gestor de actas</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo link_tag('assets/css/main.css'); ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
    <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous">
    </script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script> 
</head>
<body>

<header>
    <img src="<?php echo base_url(); ?>/assets/img/logo.png">
</header>

<div id="menu">
    <ul id="nav">
        <li><a href="<?php echo base_url(); ?>index.php/home/load_home_view">Inicio</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/home/list_minutes">Actas municipales</a>
            <ul>
                <li><a href="<?php echo base_url(); ?>index.php/home/load_insert_minutes_view">Agregar Acta municipal</a></li>
            </ul>
        </li>
        <li><a href="<?php echo base_url(); ?>index.php/home/list_articles">Control de acuerdos</a></li>
        <li><a href="<?php echo base_url(); ?>index.php/home/load_change_password_view">Cambiar contraseña</a></li>
        <?php
            if ($this->session->userdata('user_role') == 1) { //Se controla si el usuario es administrador o visita
                echo '<li><a href="' . base_url() . 'index.php/home/list_users">Usuarios</a>
                        <ul>
                            <li><a href="' . base_url() . 'index.php/home/load_insert_user_view">Crear usuario</a></li>
                        </ul>
                    </li>';
            }            
        ?>
        <li><a href="<?php echo base_url(); ?>index.php/user/logout">Cerrar sesión</a></li>
    </ul>
</div>
