<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Minutes extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('minutes_model'); //Carga el modelo.
		$this->load->helper('download');
	}
		
	public function insert_minutes()
	{

		//Reglas para los campos del formulario de agregar actas
		$this->form_validation->set_rules('minutes_number', 'número de acta', 'required',
																				array('required' => 'Debe escribir un %s.'));
		$this->form_validation->set_rules('municipal_period', 'periodo municipal', 'required',
																				array('required' => 'Debe seleccionar un %s.'));
		$this->form_validation->set_rules('session_date', 'fecha de la sesión', 'required',
																				array('required' => 'Debe seleccionar una %s.'));
		$this->form_validation->set_rules('session_number', 'número de sesión', 'required',
																				array('required' => 'Debe escribir un %s.'));
		$this->form_validation->set_rules('session_type', 'tipo de sesión', 'required',
																				array('required' => 'Debe seleccionar un %s.'));
		if (empty($_FILES['minutes_file']['name']))
		{
    		$this->form_validation->set_rules('minutes_file', 'documento', 'required',
																				array('required' => 'Debe seleccionar un %s.'));
		}
		
		if ($this->form_validation->run() == FALSE) //En caso de no ingresar algún dato carga la vista, pero con los errores de C.I.
		{
			$periods = $this->minutes_model->list_periods(); //Trae un array con la lista de periodos que hay en la B.D.
			$data['periods'] = array(); //Se crea un array "periods" que estará dentro de $data y tendrá la información que se necesita de los periodos.

        	foreach($periods as $object) { //Se usa el loop para llenar el array de los periodos solo con la información que es necesaria para la vista de insertar actas.
            	$data['periods'][] = array(
					'start_year' => $object->municipal_period_start_year,
					'period' => $object->municipal_period_start_year . ' - ' . $object->municipal_period_end_year
            	);
			}
			$this->load->view('header');
			$this->load->view('minutes/insert_minutes', $data); //Se pasa el array $data, a la vista, que contiene el arreglo periods con la información que se necesita de los periodos en el select del formulario.
			$this->load->view('footer');
		} 
		else //Ingresó todos los datos.
		{
			//Se traen los datos del acta.
			$minutes_number = $this->input->post('minutes_number');
			$municipal_period = $this->input->post('municipal_period');
			$session_date = $this->input->post('session_date');
			$session_number = $this->input->post('session_number');
			$session_type = $this->input->post('session_type');

			//Se configura el tipo y directorio de las actas.
			$config['upload_path'] = './minutes/';
			$config['allowed_types'] = 'pdf';
			$config['overwrite'] = 'true';

			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if ($this->minutes_model->validate_minutes_number($minutes_number, $municipal_period)) { //Se valida que no exista el acta que está intentando agregar.
				echo '<div style="color:red; text-align:center; margin: 20px 0;">Ya existe un acta con ese número, por favor intente de nuevo.</div>';

				$periods = $this->minutes_model->list_periods(); //Trae un array con la lista de periodos que hay en la B.D.
				$data['periods'] = array(); //Se crea un array "periods" que estará dentro de $data y tendrá la información que se necesita de los periodos.

        		foreach($periods as $object) { //Se usa el loop para llenar el array de los periodos solo con la información que es necesaria para la vista de insertar actas.
            		$data['periods'][] = array(
						'start_year' => $object->municipal_period_start_year,
						'period' => $object->municipal_period_start_year . ' - ' . $object->municipal_period_end_year
            		);
				}
				$this->load->view('header');
				$this->load->view('minutes/insert_minutes', $data); //Se pasa el array $data, a la vista, que contiene el arreglo periods con la información que se necesita de los periodos en el select del formulario.
				$this->load->view('footer');
			} else if ( ! $this->upload->do_upload('minutes_file')) {
				echo '<div style="color:red; text-align:center; margin: 20px 0;">Ha ocurrido un error con el archivo, por favor intente de nuevo.</div>';
				echo '<br>' . $this->upload->display_errors();

				$periods = $this->minutes_model->list_periods(); //Trae un array con la lista de periodos que hay en la B.D.
				$data['periods'] = array(); //Se crea un array "periods" que estará dentro de $data y tendrá la información que se necesita de los periodos.

        		foreach($periods as $object) { //Se usa el loop para llenar el array de los periodos solo con la información que es necesaria para la vista de insertar actas.
            		$data['periods'][] = array(
						'start_year' => $object->municipal_period_start_year,
						'period' => $object->municipal_period_start_year . ' - ' . $object->municipal_period_end_year
            		);
				}

				$this->load->view('header');
				$this->load->view('minutes/insert_minutes', $data); //Se pasa el array $data, a la vista, que contiene el arreglo periods con la información que se necesita de los periodos.
				$this->load->view('footer');
			} else {
				//El acta fue subida correctamente.
				$upload_data = $this->upload->data();
				$document = $upload_data['file_name'];

				$this->minutes_model->save_minutes($minutes_number, $municipal_period, $session_date, $session_number, $session_type, $document); //Envía los datos para insertarlos en la B.D.

				echo '<div style="color:#11fb11; text-align:center; margin: 20px 0;">El acta fue ingresada correctamente.</div>';
				$this->load->view('header');
				$this->load->view('minutes/insert_minutes');
				$this->load->view('footer');
			}
		}
	}

	public function list_minutes()
    {
		// Variables del Datatable
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));


        $minutes = $this->minutes_model->minutes_list();

        $data = array();

        foreach($minutes->result() as $r) {
            $data[] = array(
                $r->minutes_number,
                $r->municipal_period_start_year,
                $r->session_date,
				$r->session_number,
				$r->session_type,
				$r->document,
				'<a href="list_articles_by_minutes?minutes=' . $r->minutes_number . '&period=' . $r->municipal_period_start_year . '">Ver artículos</a>',//Link para mostrar los artículos solo de esta acta, se pasa por GET el primary key del acta.
				$r->added_by
            );
        }

        $output = array (
            "draw" => $draw,
            "recordsTotal" => $minutes->num_rows(),
            "recordsFiltered" => $minutes->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
	}
	
	public function download_file ()
	{
		$file_name = $_GET["file"];//Se traé el nombre del archivo a descargar que fue pasado por GET desde la vista.
		$file_path = './minutes/' . $file_name;
		force_download($file_path, NULL);
	}

	public function list_articles()
    {
		// Variables del Datatable
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));


        $articles = $this->minutes_model->articles_list();

        $data = array();

        foreach($articles->result() as $r) {
            $data[] = array(
                $r->minutes_number,
                $r->municipal_period_start_year,
                $r->article_number,
				$r->article_description,
				$r->have_answer == 1 ? 'Sí' : 'No',
				$r->answer_date,
				$r->answer_minutes_number,
				$r->answer_article_number,
				'<a href="edit_article?minutes_number=' . $r->minutes_number . '&period=' . $r->municipal_period_start_year . '&article_number=' . $r->article_number . '&article_description=' . $r->article_description . '&have_answer=' . $r->have_answer . '&answer_date' . $r->answer_date . '&answer_minutes_number=' . $r->answer_minutes_number . '&answer_minutes_period=' . $r->answer_minutes_period . '&answer_article_number=' . $r->answer_article_number . '">Editar</a>'//Link para editar el artículo, se pasan todos los datos del artículo por GET para cargarlos en la vista de editar.
            );
        }

        $output = array (
            "draw" => $draw,
            "recordsTotal" => $articles->num_rows(),
            "recordsFiltered" => $articles->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
	}

	public function list_articles_by_minutes ()
	{
		// Variables del Datatable
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

		$minutes = $_GET["minutes"];
		$period = $_GET["period"];
		$articles = $this->minutes_model->articles_by_minutes($minutes, $period); //Se traer filtrados los artículos de un acta en específico

        $data = array();

        foreach($articles->result() as $r) {
            $data[] = array(
                $r->minutes_number,
                $r->municipal_period_start_year,
                $r->article_number,
				$r->article_description,
				$r->have_answer == 1 ? 'Sí' : 'No',
				$r->answer_date,
				$r->answer_minutes_number,
				$r->answer_article_number,
				'<a href="edit_article?minutes_number=' . $r->minutes_number . '&period=' . $r->municipal_period_start_year . '&article_number=' . $r->article_number . '&article_description=' . $r->article_description . '&have_answer=' . $r->have_answer . '&answer_date=' . $r->answer_date . '&answer_minutes_number=' . $r->answer_minutes_number . '&answer_minutes_period=' . $r->answer_minutes_period . '&answer_article_number=' . $r->answer_article_number . '">Editar</a>'//Link para editar el artículo, se pasan todos los datos del artículo por GET para cargarlos en la vista de editar.
            );
        }

        $output = array (
            "draw" => $draw,
            "recordsTotal" => $articles->num_rows(),
            "recordsFiltered" => $articles->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
	}

	public function insert_article ()
	{
		//Reglas para el formulario donde se insertan artículos.
		$this->form_validation->set_rules('minutes_number', 'número de acta', 'required',
										array('required' => 'Debe escribir un %s.'));
		$this->form_validation->set_rules('period', 'periodo', 'required',
										array('required' => 'Debe escribir un %s.'));
		$this->form_validation->set_rules('article_number', 'artículo', 'required',
										array('required' => 'Debe escribir un número de %s.'));
		$this->form_validation->set_rules('article_description', 'descripción', 'required',
										array('required' => 'Debe escribir una %s.'));
		$this->form_validation->set_rules('have_answer', 'respuesta', 'required',
										array('required' => 'Debe seleccionar una opción.'));

		if ($this->form_validation->run() == FALSE) //En caso de ingresar algún dato de manera incorrecta carga la vista, pero con los errores de C.I.
		{
			//En caso de equivocarse y no ingresar algún dato es necesario volver a pasar los datos del acta a la que se quieren agregar artículos para que se puedan cargar en la vista de nuevo, por esa razón fue que en el Home Controller, cuando se redireccionó a la vista de "Insertar Artículos" también se guardó en sesión los datos del acta.
			$data['minutes'] = $this->session->userdata('minutes');
			$data['period'] = $this->session->userdata('period');

			$periods = $this->minutes_model->list_periods(); //Trae un array con la lista de periodos que hay en la B.D.
			$data['periods'] = array(); //Se crea un array "periods" que estará dentro de $data y tendrá la información que se necesita de los periodos.

        	foreach($periods as $object) { //Se usa el loop para llenar el array de los periodos solo con la información que es necesaria para la vista de insertar artículos.
            $data['periods'][] = array(
				'start_year' => $object->municipal_period_start_year,
				'period' => $object->municipal_period_start_year . ' - ' . $object->municipal_period_end_year
            );
			}
			$this->load->view('header');
			$this->load->view('minutes/insert_article', $data);
			$this->load->view('footer');
		} 
		else //Ingresó todos los datos.
		{
			$minutes_number = $this->input->post('minutes_number');
			$period = $this->input->post('period');
			$article_number = $this->input->post('article_number');
			$article_description = $this->input->post('article_description');
			$have_answer = $this->input->post('have_answer');
			$answer_date = $this->input->post('answer_date');
			$answer_minutes_number = $this->input->post('answer_minutes_number');
			$municipal_period = $this->input->post('municipal_period');
			$answer_article_number = $this->input->post('answer_article_number');

			if ($this->minutes_model->validate_article($minutes_number, $period, $article_number)) {
				echo '<div style="color:red; text-align:center; margin: 20px 0;">Ya existe este número de artículo en el acta indicada, intente de nuevo.</div>';

				//En caso de equivocarse e ingresar un número de acta que ya existe es necesario volver a pasar los datos del acta a la que se quieren agregar artículos para que se puedan cargar en la vista de nuevo, por esa razón fue que en el Home Controller, cuando se redireccionó a la vista de "Insertar Artículos" también se guardó en sesión los datos del acta.
				$data['minutes'] = $this->session->userdata('minutes');
				$data['period'] = $this->session->userdata('period');

				$this->load->view('header');
				$this->load->view('minutes/insert_article', $data);
				$this->load->view('footer');
			} else {
				$this->minutes_model->insert_article($minutes_number, $period, $article_number, $article_description, $have_answer, $answer_date, $answer_minutes_number, $municipal_period, $answer_article_number);

				echo '<div style="color:#11fb11; text-align:center; margin: 20px 0;">El artículo fue ingresado correctamente.</div>';

				$data['minutes'] = $this->session->userdata('minutes');
				$data['period'] = $this->session->userdata('period');
				$this->load->view('header');
				$this->load->view('minutes/list_articles_by_minutes', $data);
				$this->load->view('footer');
			}
		}
	}

	public function edit_article ()
	{
		//Reglas para el formulario de editar artículo.
		$this->form_validation->set_rules('minutes_number', 'número de acta', 'required',
										array('required' => 'Debe escribir un %s.'));
		$this->form_validation->set_rules('period', 'periodo', 'required',
										array('required' => 'Debe escribir un %s.'));
		$this->form_validation->set_rules('article_number', 'artículo', 'required',
										array('required' => 'Debe escribir un número de %s.'));
		$this->form_validation->set_rules('article_description', 'descripción', 'required',
										array('required' => 'Debe escribir una %s.'));
		$this->form_validation->set_rules('have_answer', 'respuesta', 'required',
										array('required' => 'Debe seleccionar una opción.'));

		if ($this->form_validation->run() == FALSE) //En caso de ingresar algún dato de manera incorrecta carga la vista, pero con los errores de C.I.
		{
			//En caso de equivocarse y no ingresar algún dato es necesario volver a pasar los datos del artículo que se quiere editar para que se puedan cargar en la vista de nuevo, por esa razón fue que en el Home Controller, cuando se redireccionó a la vista de "Editar Artículos" también se guardó en sesión los datos de ese artículo.
			$data['minutes_number'] = $this->session->userdata('minutes');
			$data['period'] = $this->session->userdata('period');
			$data['article_number'] = $this->session->userdata('article_number');
			$data['article_description'] = $this->session->userdata('article_description');

			$periods = $this->minutes_model->list_periods(); //Trae un array con la lista de periodos que hay en la B.D.
			$data['periods'] = array(); //Se crea un array "periods" que estará dentro de $data y tendrá la información que se necesita de los periodos.

        	foreach($periods as $object) { //Se usa el loop para llenar el array de los periodos solo con la información que es necesaria para la vista de editar artículos.
            $data['periods'][] = array(
				'start_year' => $object->municipal_period_start_year,
				'period' => $object->municipal_period_start_year . ' - ' . $object->municipal_period_end_year
            );
			}
			$this->load->view('header');
			$this->load->view('minutes/edit_article', $data);
			$this->load->view('footer');
		} 
		else //Ingresó todos los datos.
		{
			$minutes_number = $this->input->post('minutes_number');
			$period = $this->input->post('period');
			$article_number = $this->input->post('article_number');
			$article_description = $this->input->post('article_description');
			$have_answer = $this->input->post('have_answer');
			$answer_date = $this->input->post('answer_date');
			$answer_minutes_number = $this->input->post('answer_minutes_number');
			$municipal_period = $this->input->post('municipal_period');
			$answer_article_number = $this->input->post('answer_article_number');

			$this->minutes_model->edit_article($minutes_number, $period, $article_number, $article_description, $have_answer, $answer_date, $answer_minutes_number, $municipal_period, $answer_article_number);

			$data['minutes'] = $minutes_number;
			$data['period'] = $period;

			$this->load->view('header');
			$this->load->view('minutes/list_articles_by_minutes', $data);
			$this->load->view('footer');
		}
	}

}