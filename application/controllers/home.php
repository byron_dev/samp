<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('minutes_model'); //Carga el modelo de actas.
	}

	public function load_home_view() //Carga la vista de inicio.
	{
		$this->load->view('header');
		$this->load->view('home');
		$this->load->view('footer');
	}

	public function load_insert_minutes_view() //Carga la vista de insertar actas.
	{
		$periods = $this->minutes_model->list_periods(); //Trae un array con la lista de periodos que hay en la B.D.
		$data['periods'] = array(); //Se crea un array "periods" que estará dentro de $data y tendrá la información que se necesita de los periodos.

        foreach($periods as $object) { //Se usa el loop para llenar el array de los periodos solo con la información que es necesaria para la vista de insertar actas.
            $data['periods'][] = array(
				'start_year' => $object->municipal_period_start_year,
				'period' => $object->municipal_period_start_year . ' - ' . $object->municipal_period_end_year
            );
		}
		
		$this->load->view('header');
		$this->load->view('minutes/insert_minutes', $data); //Se pasa el array $data, a la vista, que contiene el arreglo periods con la información que se necesita de los periodos.
		$this->load->view('footer');
	}
	
	public function load_change_password_view() //Carga la vista donde se cambia la contraseña
	{
		$this->load->view('header');
		$this->load->view('user/change_password');
		$this->load->view('footer');
	}

	public function load_insert_user_view() //Carga la vista donde se cambia la contraseña
	{
		$this->load->view('header');
		$this->load->view('user/insert_user');
		$this->load->view('footer');
	}

	public function list_users()
	{
		$this->load->view('header');
		$this->load->view('user/list_users');
		$this->load->view('footer');
	}

	public function list_minutes()
	{
		$this->load->view('header');
		$this->load->view('minutes/list_minutes');
		$this->load->view('footer');
	}

	public function list_articles()
	{
		$this->load->view('header');
		$this->load->view('minutes/list_articles');
		$this->load->view('footer');
	}

	public function list_articles_by_minutes()
	{
		$minutes = $_GET["minutes"];
		$period = $_GET["period"];
		//Arreglo con la información del acta de la que se quieren extraer los artículos
		$data = array('minutes' => $minutes);
		$data += array('period' => $period);
		$this->load->view('header');
		$this->load->view('minutes/list_articles_by_minutes', $data);
		$this->load->view('footer');
	}

	public function insert_article()
	{
		$minutes = $_GET["minutes"];
		$period = $_GET["period"];
		//Se ponen en sessión los valores del acta y el periodo para control de errores
		$this->session->set_userdata('minutes', $minutes);
		$this->session->set_userdata('period', $period);
		//Arreglo con la información del acta de la que se quieren agregar artículos
		$data = array('minutes' => $minutes);
		$data += array('period' => $period);
		$this->load->view('header');
		$this->load->view('minutes/insert_article', $data);
		$this->load->view('footer');
	}

	public function edit_article ()
	{
		$minutes_number = $_GET["minutes_number"];
		$period = $_GET["period"];
		$article_number = $_GET["article_number"];
		$article_description = $_GET["article_description"];

		//Se ponen en sessión los valores del acta y el periodo ya que solo se pasan una vez y si el usuario ingresa mal un dato, al cargar de nuevo la página se perderán los datos, en cambio si se tienen en sesión pueden ser utilizados en cualquier momento.
		$this->session->set_userdata('minutes', $minutes_number);
		$this->session->set_userdata('period', $period);
		$this->session->set_userdata('article_number', $article_number);
		$this->session->set_userdata('article_description', $article_description);

		$have_answer = $_GET["have_answer"];
		$answer_date = $_GET["answer_date"];
		$answer_minutes_number = $_GET["answer_minutes_number"];
		$answer_minutes_period = $_GET["answer_minutes_period"];
		$answer_article_number = $_GET["answer_article_number"];

		$data = array('minutes_number' => $minutes_number);
		$data += array('period' => $period);
		$data += array('article_number' => $article_number);
		$data += array('article_description' => $article_description);
		$data += array('have_answer' => $have_answer);
		$data += array('answer_date' => $answer_date);
		$data += array('answer_minutes_number' => $answer_minutes_number);
		$data += array('answer_minutes_period' => $answer_minutes_period);
		$data += array('answer_article_number' => $answer_article_number);

		$periods = $this->minutes_model->list_periods(); //Trae un array con la lista de periodos que hay en la B.D.
		$data['periods'] = array(); //Se crea un array "periods" que estará dentro de $data y tendrá la información que se necesita de los periodos.

        foreach($periods as $object) { //Se usa el loop para llenar el array de los periodos solo con la información que es necesaria para la vista de insertar actas.
            $data['periods'][] = array(
				'start_year' => $object->municipal_period_start_year,
				'period' => $object->municipal_period_start_year . ' - ' . $object->municipal_period_end_year
            );
		}

		$this->load->view('header');
		$this->load->view('minutes/edit_article', $data);
		$this->load->view('footer');
	}
}