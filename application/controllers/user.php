<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('user_model'); //Carga el modelo.
    }
	
	public function index()
	{
		//Se establecen las reglas para los campos del formulario de log in.
		$this->form_validation->set_rules('username', 'usuario', 'required',
										array('required' => 'Debe escribir su %s.'));
		$this->form_validation->set_rules('password', 'contraseña', 'required',
										array('required' => 'Debe escribir su %s.'));

		if ($this->form_validation->run() == FALSE) //Si no ingresó algún dato vuelve a cargar la vista de login, pero con los errores de C.I.
		{
			$this->load->view('header_login');
			$this->load->view('user/login');
			$this->load->view('footer');
		}
		else
		{
			//Trae los datos pasados por POST
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$result = $this->user_model->login($username, $password); //Envía los datos y realiza la validación del usuario.

			if($result) //Valida si el modelo retornó TRUE (Si existe el usuario y los credenciales son correctos) o FALSE (El usuario no existe o existe, pero los credenciales son incorrectos).
  			{
				if ($password == "123") { //Si es la primera vez que ingresa se le pide que cambie la contraseña.
					echo '<div style="background-color: #2F367F; text-align: center; color: yellow; padding: 20px 0;">Bienvenido, ' . $this->session->userdata('user_name') . ', por favor cambie su contraseña antes de iniciar.</div>';
					$this->load->view('header');
					$this->load->view('user/change_password');
					$this->load->view('footer');
				} else { //Si no, lo envía a la página de inicio.
					$this->load->view('header');
					$this->load->view('home');
					$this->load->view('footer');
				}
   			}
   			else
   			{	
				echo '<div style="color:red; text-align:center; margin: 20px 0;">Los credenciales ingresados son incorrectos, intente de nuevo.</div>';
				$this->load->view('header_login');
				$this->load->view('user/login');
				$this->load->view('footer');
   			}
		}
	}

	public function logout() 
	{
		$this->user_model->logout(); //Llama la función del modelo que elimina los datos de la sesión.
		$this->load->view('header_login');
		$this->load->view('user/login');
		$this->load->view('footer');
	}

	public function change_password()
	{
		//Se definen las reglas para los campos del cambio de contraseña.
		$this->form_validation->set_rules('new_password', 'nueva contraseña', 'required|min_length[5]|max_length[12]|callback_check_password',
										array('required' => 'Debe escribir su %s.', 'min_length' => 'La contraseña debe tener al menos 5 caracteres.', 'max_length' => 'La contraseña debe tener máximo 12 caracteres.'));
		$this->form_validation->set_rules('password_confirmation', 'confirmar su nueva contraseña', 'required|matches[new_password]',
										array('required' => 'Debe %s.', 'matches' => 'La nueva contraseña y su confirmación no coinciden.'));

		if ($this->form_validation->run() == FALSE) //En caso de ingresar algún dato de manera incorrecta carga la vista, pero con los errores de C.I.
		{
			$this->load->view('header');
			$this->load->view('user/change_password');
			$this->load->view('footer');
		} 
		else //De ingresar una nueva contraseña válida lo redirecciona a la página de inicio y actualiza su contraseña en la B.D.
		{
			$password = $this->input->post('new_password');
			$this->user_model->change_password($password); //Envía los datos para el cambio de contraseña
			echo '<div style="color:#11fb11; text-align:center; margin: 20px 0;">La contraseña ha sido actualizada correctamente.</div>';
			$this->load->view('header');
			$this->load->view('home');
			$this->load->view('footer');
		}
	}

	public function check_password($password)
	{
		if (!preg_match('/^[a-z0-9]*$/i',$password)) { //Se verifica que no tenga caractéres especiales
			$this->form_validation->set_message('check_password', 'La contraseña solo puede tener letras y números.');
			return false;
		} else if (!preg_match('`[a-z]`',$password)) { //Se verifica que al menos tenga una letra minúscula
			$this->form_validation->set_message('check_password', 'La contraseña debe tener al menos una letra minúscula.');
			return false;
		} else if (!preg_match('`[A-Z]`',$password)) { //Se verifica que al menos tenga una letra mayúscula
			$this->form_validation->set_message('check_password', 'La contraseña debe tener al menos una letra mayúscula.');
			return false;
		} else if (!preg_match('`[0-9]`',$password)) { //Se verifica que tenga al menos un número
			$this->form_validation->set_message('check_password', 'La contraseña debe tener al menos un número.');
			return false;
		} else {
			return true;
		}
	}

	public function insert_user()
	{
		//Reglas para el formulario de insertar usuario.
		$this->form_validation->set_rules('username', 'nombre', 'required',
										array('required' => 'Debe escribir un %s.'));
		$this->form_validation->set_rules('lastname', 'apellido', 'required',
										array('required' => 'Debe escribir un %s.'));
		$this->form_validation->set_rules('nickname', 'usuario', 'required',
										array('required' => 'Debe escribir un nombre de %s.'));
		$this->form_validation->set_rules('role', 'rol', 'required',
										array('required' => 'Debe seleccionar un %s.'));

		if ($this->form_validation->run() == FALSE) //En caso de ingresar algún dato de manera incorrecta carga la vista, pero con los errores de C.I.
		{
			$this->load->view('header');
			$this->load->view('user/insert_user');
			$this->load->view('footer');
		} 
		else //Ingresó todos los datos.
		{
			$user_name = $this->input->post('username');
			$user_last_name = $this->input->post('lastname');
			$user_nickname = $this->input->post('nickname');
			$user_rol = $this->input->post('role');

			if ($this->user_model->validate_user_nickname($user_nickname)) {
				echo '<div style="color:red; text-align:center; margin: 20px 0;">Ya existe una persona utilizando ese usuario, por favor intente de nuevo.</div>';
				$this->load->view('header');
				$this->load->view('user/insert_user');
				$this->load->view('footer');
			} else {
				$this->user_model->insert_user($user_name, $user_last_name, $user_nickname, $user_rol); //Envía los datos para insertarlos en la B.D.

				echo '<div style="color:#11fb11; text-align:center; margin: 20px 0;">El usuario fue ingresado correctamente.</div>';
				$this->load->view('header');
				$this->load->view('user/insert_user');
				$this->load->view('footer');
			}
		}
	}

	public function list_users()
    {
		// Variables del Datatable
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));


        $users = $this->user_model->users_list();

        $data = array();

        foreach($users->result() as $r) {
            $data[] = array(
                $r->user_name,
                $r->user_last_name,
                $r->user_nickname,
                $r->user_role
            );
        }

        $output = array (
            "draw" => $draw,
            "recordsTotal" => $users->num_rows(),
            "recordsFiltered" => $users->num_rows(),
            "data" => $data
        );
        echo json_encode($output);
        exit();
    }

}
