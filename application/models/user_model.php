<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function login ($username, $password) 
    {
        //Busca un en la B.D. un registro con esos credenciales.
        $this->db->where('user_nickname', $username);
        $this->db->where('user_password', sha1($password));//Se encripta la contraseña
        $this ->db->limit(1);
        $result = $this->db->get('system_user');
        $data = $result->result();

        if ($data) //Valida si existe ese usuario.
        {
            $user_data = array( //Se crea el array con la información del usuario que va a ser utilizada para la sesión.
                'user_nickname'  => $data[0]->user_nickname,
                'user_name' => $data[0]->user_name,
                'user_role' => $data[0]->user_role,
                'logged_in' => TRUE
            );
        
            $this->session->set_userdata($user_data); //Se almacena la información en sesión.
            return true; //El usuario si existe.
        } else 
        {
            return false; //El usuario no existe.
        }

    }

    public function validate_user_nickname ($user_nickname)
    {
        $this->db->where('user_nickname', $user_nickname);
        $this ->db->limit(1);
        $result = $this->db->get('system_user');
        $data = $result->result();

        if ($data) //Valida si existe ese usuario.
        {
            return true; //El usuario si existe.
        } else 
        {
            return false; //El usuario no existe.
        }
    }
    
    public function logout()
    {
        $this->session->unset_userdata('username'); //Elimina los datos de la sesión.
    }

    public function change_password ($password) 
    {
        $this->db->set('user_password', sha1($password)); //Se indica cual campo hay que actualizar y por cual valor
        $this->db->where('user_nickname', $this->session->userdata('user_nickname')); //Se especifica que en la tupla del usuario actual
        $this->db->update('system_user'); //Se dice en cual tabla de la B.D.
    }

    public function insert_user ($user_name, $user_last_name, $user_nickname, $user_role)
    {
        //Se crea el array con los datos a almacenar en la B.D.
        $data = array(
            'user_name' => $user_name,
            'user_last_name' => $user_last_name,
            'user_nickname' => $user_nickname,
            'user_password' => sha1('123'),
            'user_role' => $user_role
        );

        $this->db->insert('system_user', $data);
    }

    public function users_list()
    {
        return $this->db->get("system_user");
    }

}