<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Minutes_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function list_periods() 
    {
        //Busca un en la B.D. la lista de periodos municipales.
        $result = $this->db->get("municipal_period");
        return $periods = $result->result();
    }

    public function validate_minutes_number ($minutes_number, $municipal_period)
    {
        $this->db->where('minutes_number', $minutes_number);
        $this->db->where('municipal_period_start_year', $municipal_period);
        $this ->db->limit(1);
        $result = $this->db->get('minutes');
        $data = $result->result();

        if ($data) //Valida si existe un acta con esas características.
        {
            return true; //El acta si existe.
        } else 
        {
            return false; //El acta no existe.
        }
    }

    function save_minutes($minutes_number, $municipal_period, $session_date, $session_number, $session_type, $document){
		$data = array(
            'minutes_number' => $minutes_number,
            'municipal_period_start_year' => $municipal_period,
            'session_date' => $session_date,
            'session_number' => $session_number,
            'session_type' => $session_type,
            'document' => $document,
            'added_by' => $this->session->userdata('user_name')
        );

        $this->db->insert('minutes', $data);
    }
    
    public function minutes_list()
    {
        return $this->db->get("minutes");
    }

    public function articles_list()
    {
        return $this->db->get("minutes_article");
    }

    public function articles_by_minutes ($minutes, $period)
    {
        $this->db->where('minutes_number', $minutes);
        $this->db->where('municipal_period_start_year', $period);
        return $this->db->get('minutes_article');
    }

    public function edit_article ($minutes_number, $period, $article_number, $article_description, $have_answer, $answer_date, $answer_minutes_number, $municipal_period, $answer_article_number)
    {
        $data = array(
            'minutes_number' => $minutes_number,
            'municipal_period_start_year'  => $period,
            'article_number'  => $article_number,
            'article_description'  => $article_description,
            'have_answer'  => $have_answer,
            'answer_date'  => $answer_date,
            'answer_minutes_number'  => $answer_minutes_number,
            'answer_minutes_period'  => $municipal_period,
            'answer_article_number'  => $answer_article_number
        );

        $this->db->replace('minutes_article', $data);
    }

    public function validate_article ($minutes_number, $period, $article_number)
    {
        $this->db->where('minutes_number', $minutes_number);
        $this->db->where('municipal_period_start_year', $period);
        $this->db->where('article_number', $article_number);
        $this ->db->limit(1);
        $result = $this->db->get('minutes_article');
        $data = $result->result();

        if ($data) //Valida si existe ese usuario.
        {
            return true; //El usuario si existe.
        } else 
        {
            return false; //El usuario no existe.
        }
    }

    public function insert_article ($minutes_number, $period, $article_number, $article_description, $have_answer, $answer_date, $answer_minutes_number, $municipal_period, $answer_article_number)
    {
        $data = array(
            'minutes_number' => $minutes_number,
            'municipal_period_start_year'  => $period,
            'article_number'  => $article_number,
            'article_description'  => $article_description,
            'have_answer'  => $have_answer,
            'answer_date'  => $answer_date,
            'answer_minutes_number'  => $answer_minutes_number,
            'answer_minutes_period'  => $municipal_period,
            'answer_article_number'  => $answer_article_number
        );

        $this->db->insert('minutes_article', $data);
    }

}